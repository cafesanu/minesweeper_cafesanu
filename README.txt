Hello!

If you want to download this project, please click on "get source".

This game was built for Spanish speakers, but the essence is the same and you don't really need
to know anything about Spanish in order to understand the GUI.


This game was built on Borland C++ Builder 6.
This game should run on any windows machine, although it was build and optimized for Window XP.
I have recently tested it on Windows 7 machines and it still runs pretty good although some GUI
 aspects seemed not to work so well, but all in all it is still usable and great to play for fun!

Once you download the project:

If you want to read the code:
    +GUI code:   ./WinMine_CAFESANU_Code_1.0/Interfaz
    +Logic code: ./WinMine_CAFESANU_Code_1.0/Kernel

If you want to play the game:
    +Run         ./WinMine_CAFESANU_Code_1.0/WinMineCAFESANU

If you want to actually install the game
    +Run         ./WinMine_CAFESANU_Installer/WMC.exe

Any questions please feel free to reach me at cafesanu [at] gmail
